<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap&subset=cyrillic" rel="stylesheet">
        
        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="fonts/fontawesome5/css/all.min.css">
        <link rel="stylesheet" href="css/main.css">

    </head>
    <body>

        <div class="page">

            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <a class="header__logo" href="/">
                            <img src="img/logo.png" class="img-fluid" alt="">
                        </a>
                        <ul class="header__nav">
                            <li><a href="#"><span>Home</span></a></li>
                            <li><a href="#"><span>Company info</span></a></li>
                            <li class="active"><a href="#"><span>How to start?</span></a></li>
                        </ul>
                    </div>
                </div>
            </header>

            <section class="promo">
                <div class="container">
                    <div class="promo__wrap">
                        <div class="promo__logo">
                            <img src="img/promo__logo.png" class="img-fluid" alt="">
                        </div>
                        <div class="promo__content">
                            <h1>What is Vexa Global?</h1>
                            <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on! You will be able to have your profile, just like mine. You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started.</p>
                            <p class="mb_50">You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started.</p>
                            <a href="#" class="btn btn_border btn_lg text-uppercase">get started</a>
                        </div>
                    </div>
                </div>
            </section>

            <section class="main main_border">
                <div class="container">

                    <div class="help_one">
                        <div class="help_one__image">
                            <img src="img/help__image_01.png" class="img-fluid" alt="">
                        </div>
                        <div class="help_one__content">
                            <div class="help_one__text">
                                <h3>1. Press “Register” button</h3>
                                <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on!</p>
                            </div>
                        </div>
                    </div>

                    <div class="help_two">
                        <div class="help_two__image">
                            <img src="img/help__image_02.png" class="img-fluid" alt="">
                        </div>
                        <div class="help_two__content">
                            <h2>2. Register Popup</h2>
                            <p>You will not get this gift anywhere else, but here! Once you register here with your email address</p>
                            <ol class="help_list">
                                <li>
                                    <h4>E-mail</h4>
                                    <p>You will not get this gift anywhere else, but here! Once you register here with your email address</p>
                                </li>
                                <li>
                                    <h4>Password</h4>
                                    <p>You will not get this gift anywhere else, but here!</p>
                                </li>
                                <li>
                                    <h4>Repeat password</h4>
                                    <p>You will not get this gift anywhere else, but here!</p>
                                </li>
                                <li>
                                    <h4>Upline</h4>
                                    <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on! You will be able to have your profile, just like mine. You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started.</p>
                                </li>
                                <li>
                                    <h4>Inviter</h4>
                                    <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on! You will be able to have your profile, just like mine.</p>
                                    <p>You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started.</p>
                                </li>
                            </ol>
                        </div>
                    </div>

                    <div class="help_three">
                        <div class="help_three__image">
                            <img src="img/help__image_03.png" class="img-fluid" alt="">
                        </div>
                        <div class="help_three__content">
                            <div class="help_three__text">
                                <h3>3. Confirm Registration</h3>
                                <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on!</p>
                            </div>
                        </div>
                    </div>

                    <div class="help_four">
                        <div class="help_four__image">
                            <img src="img/help__image_04.png" class="img-fluid" alt="">
                        </div>
                        <div class="help_four__content">
                            <h2>4. Activate your account</h2>
                            <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on!</p>
                            <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on! You will be able to have your profile, just like mine.</p>
                            <ol class="help_list">
                                <li>
                                    <h4>Nickname</h4>
                                    <p>You will not get this gift anywhere else, but here! Once you register here with your email address</p>
                                </li>
                                <li>
                                    <h4>Code from email</h4>
                                    <p>You will not get this gift anywhere else, but here!</p>
                                </li>
                            </ol>
                        </div>
                    </div>

                    <div class="help_five">
                        <div class="help_five__image">
                            <img src="img/help__image_05.png" class="img-fluid hide-xs-only hide-sm-only hide-md-only" alt="">
                            <img src="img/help__image_05_mobile.png" class="img-fluid hide-lg-only hide-xl" alt="">
                        </div>
                        <div class="help_five__content">
                            <ul class="help_five__legend">
                                <li>
                                    <h4>Registration code</h4>
                                    <p>You will not get this gift anywhere else, but here! Once you register here with your email address</p>
                                </li>
                                <li>
                                    <h4>Activation link</h4>
                                    <p>You will not get this gift anywhere else, but here!</p>
                                </li>
                            </ul>
                            <div class="warning">If you close the pop-up window, then by clicking activation link, you will automatically receive this window only with the entered code on website</div>
                        </div>
                    </div>

                </div>
            </section>


            <footer class="footer">
                <div class="container">
                    <div class="footer__row">
                        <a href="#" class="footer__logo">
                            <img src="img/footer__logo.png" class="img-fluid" alt="">
                        </a>
                        <ul class="footer__nav">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Company info</a></li>
                            <li><a href="#">How to start?</a></li>
                        </ul>
                        <ul class="footer__social">
                            <li><a href="#"><i class="fab fa-vk"></i></a></li>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-whatsapp"></i></a></li>
                            <li><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
                        </ul>
                    </div>
                    <div class="footer__text">
                        © 2019 VexaGlobal.com<br/>
                        All rights reserved.
                    </div>
                </div>
            </footer>

        </div>


        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>

