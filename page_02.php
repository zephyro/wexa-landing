<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap&subset=cyrillic" rel="stylesheet">
        
        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="fonts/fontawesome5/css/all.min.css">
        <link rel="stylesheet" href="css/main.css">

    </head>
    <body>

        <div class="page">

            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <a class="header__logo" href="/">
                            <img src="img/logo.png" class="img-fluid" alt="">
                        </a>
                        <ul class="header__nav">
                            <li class="active"><a href="#"><span>Home</span></a></li>
                            <li><a href="#"><span>Company info</span></a></li>
                            <li><a href="#"><span>How to start?</span></a></li>
                        </ul>
                    </div>
                </div>
            </header>

            <section class="promo">
                <div class="container">
                    <div class="promo__wrap">
                        <div class="promo__logo">
                            <img src="img/promo__logo.png" class="img-fluid" alt="">
                        </div>
                        <ul class="promo__list">
                            <li>
                                <h3>What is Vexa Global?</h3>
                                <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on! You will be able to have your profile, just like mine.</p>
                            </li>
                            <li>
                                <h3>Why better is invest into Vexa Global than keep your money into bank account?</h3>
                                <p>
                                    As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on! You will be able to have your profile, just like mine.<br/>
                                    You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started.
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>

            <section class="tariff">
                <div class="container">
                    <div class="tariff__row">

                        <div class="tariff__item">
                            <div class="tariff__item_icon">
                                <img src="img/tariff_logo__01.png" class="img-fluid" alt="">
                            </div>
                            <div class="tariff__item_title tariff_bs">BASIC</div>
                            <div class="tariff__item_price">$50 - $999</div>
                            <div class="tariff__item_info">130 business days</div>
                            <div class="tariff__item_button">
                                <a href="#" class="btn btn_shadow text-uppercase">buy basic</a>
                            </div>
                        </div>

                        <div class="tariff__item">
                            <div class="tariff__item_icon">
                                <img src="img/tariff_logo__02.png" class="img-fluid" alt="">
                            </div>
                            <div class="tariff__item_title tariff_pr">PREMIUM</div>
                            <div class="tariff__item_price">$1,000 - $9,999</div>
                            <div class="tariff__item_info">150 business days</div>
                            <div class="tariff__item_button">
                                <a href="#" class="btn btn_shadow text-uppercase">buy PREMUIM</a>
                            </div>
                        </div>

                        <div class="tariff__item">
                            <div class="tariff__item_icon">
                                <img src="img/tariff_logo__03.png" class="img-fluid" alt="">
                            </div>
                            <div class="tariff__item_title tariff_ex">EXCLUSIVE</div>
                            <div class="tariff__item_price">$10,000 - $100,000</div>
                            <div class="tariff__item_info">180 business days</div>
                            <div class="tariff__item_button">
                                <a href="#" class="btn btn_shadow text-uppercase">buy EXCLUSIVE</a>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="main main_border">
                <div class="container">
                    <h2>Career</h2>

                    <div class="career__wrap">
                        <div class="career">
                            <div class="career__title">
                                <strong>2 LEVEL</strong>
                                <span>BEGINNER</span>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_01.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$200</strong>
                                    <span>Bonus</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_02.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$10,000</strong>
                                    <span>Sales</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_03.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$100</strong>
                                    <span>Package</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_04.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>8.00%</strong>
                                    <span>Percent</span>
                                </div>
                            </div>
                        </div>
                        <div class="career">
                            <div class="career__title">
                                <strong>3 LEVEL</strong>
                                <span>JUNIOR AGENT</span>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_01.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$500</strong>
                                    <span>Bonus</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_02.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$30,000</strong>
                                    <span>Sales</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_03.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$200</strong>
                                    <span>Package</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_04.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>10.00%</strong>
                                    <span>Percent</span>
                                </div>
                            </div>
                        </div>
                        <div class="career">
                            <div class="career__title">
                                <strong>4 LEVEL</strong>
                                <span>AGENT</span>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_01.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$1,000</strong>
                                    <span>Bonus</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_02.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$60,000</strong>
                                    <span>Sales</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_03.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$500</strong>
                                    <span>Package</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_04.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>11.00%</strong>
                                    <span>Percent</span>
                                </div>
                            </div>
                        </div>
                        <div class="career">
                            <div class="career__title">
                                <strong>5 LEVEL</strong>
                                <span>SENIOR AGENT</span>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_01.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$2,000</strong>
                                    <span>Bonus</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_02.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>120,000</strong>
                                    <span>Sales</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_03.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$1,000</strong>
                                    <span>Package</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_04.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>12.00%</strong>
                                    <span>Percent</span>
                                </div>
                            </div>
                        </div>
                        <div class="career">
                            <div class="career__title">
                                <strong>6 LEVEL</strong>
                                <span>JUNIOR MANAGER</span>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_01.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$5,000</strong>
                                    <span>Bonus</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_02.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>250,000</strong>
                                    <span>Sales</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_03.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$2,500</strong>
                                    <span>Package</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_04.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>13.00%</strong>
                                    <span>Percent</span>
                                </div>
                            </div>
                        </div>
                        <div class="career">
                            <div class="career__title">
                                <strong>7 LEVEL</strong>
                                <span>MANAGER</span>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_01.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$0</strong>
                                    <span>Bonus</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_02.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>500,000</strong>
                                    <span>Sales</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_03.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$5,000</strong>
                                    <span>Package</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_04.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>14.00%</strong>
                                    <span>Percent</span>
                                </div>
                            </div>
                        </div>
                        <div class="career">
                            <div class="career__title">
                                <strong>3 LEVEL</strong>
                                <span>JUNIOR AGENT</span>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_01.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$500</strong>
                                    <span>Bonus</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_02.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$30,000</strong>
                                    <span>Sales</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_03.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$200</strong>
                                    <span>Package</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_04.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>10.00%</strong>
                                    <span>Percent</span>
                                </div>
                            </div>
                        </div>
                        <div class="career">
                            <div class="career__title">
                                <strong>4 LEVEL</strong>
                                <span>AGENT</span>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_01.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$1,000</strong>
                                    <span>Bonus</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_02.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$60,000</strong>
                                    <span>Sales</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_03.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$500</strong>
                                    <span>Package</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_04.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>11.00%</strong>
                                    <span>Percent</span>
                                </div>
                            </div>
                        </div>
                        <div class="career">
                            <div class="career__title">
                                <strong>5 LEVEL</strong>
                                <span>SENIOR AGENT</span>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_01.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$2,000</strong>
                                    <span>Bonus</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_02.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>120,000</strong>
                                    <span>Sales</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_03.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>$1,000</strong>
                                    <span>Package</span>
                                </div>
                            </div>
                            <div class="career__item">
                                <div class="career__item_icon">
                                    <img src="img/career__icon_04.png" class="img-fluid" alt="">
                                </div>
                                <div class="career__item_text">
                                    <strong>12.00%</strong>
                                    <span>Percent</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="main main_border">
                <div class="container">
                    <h2>FAQ</h2>

                    <div class="faq_list">

                        <div class="faq open">
                            <div class="faq__question">
                                <i class="fas fa-angle-down"></i>
                                <span>What You Get With Vexa?</span>
                            </div>
                            <div class="faq__answer" style="display: block">
                                <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on! You will be able to have your profile, just like mine. You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started.</p>
                                <p>You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started. </p>
                            </div>
                        </div>

                        <div class="faq">
                            <div class="faq__question">
                                <i class="fas fa-angle-down"></i>
                                <span>Let Me Help You Get a Head Start</span>
                            </div>
                            <div class="faq__answer">
                                <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on! You will be able to have your profile, just like mine. You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started.</p>
                                <p>You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started. </p>
                            </div>
                        </div>

                        <div class="faq">
                            <div class="faq__question">
                                <i class="fas fa-angle-down"></i>
                                <span>How to Activate your Account?</span>
                            </div>
                            <div class="faq__answer">
                                <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on! You will be able to have your profile, just like mine. You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started.</p>
                                <p>You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started. </p>
                            </div>
                        </div>

                        <div class="faq">
                            <div class="faq__question">
                                <i class="fas fa-angle-down"></i>
                                <span>What if I didn't receive an activation email?</span>
                            </div>
                            <div class="faq__answer">
                                <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on! You will be able to have your profile, just like mine. You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started.</p>
                                <p>You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started. </p>
                            </div>
                        </div>

                        <div class="faq">
                            <div class="faq__question">
                                <i class="fas fa-angle-down"></i>
                                <span>Let Me Help You Get a Head Start</span>
                            </div>
                            <div class="faq__answer">
                                <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on! You will be able to have your profile, just like mine. You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started.</p>
                                <p>You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started. </p>
                            </div>
                        </div>

                        <div class="faq">
                            <div class="faq__question">
                                <i class="fas fa-angle-down"></i>
                                <span>How to Activate your Account?</span>
                            </div>
                            <div class="faq__answer">
                                <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on! You will be able to have your profile, just like mine. You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started.</p>
                                <p>You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started. </p>
                            </div>
                        </div>

                        <div class="faq">
                            <div class="faq__question">
                                <i class="fas fa-angle-down"></i>
                                <span>What if I didn't receive an activation email?</span>
                            </div>
                            <div class="faq__answer">
                                <p>As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on! You will be able to have your profile, just like mine. You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started.</p>
                                <p>You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started. </p>
                            </div>
                        </div>

                    </div>

                </div>
            </section>

            <footer class="footer">
                <div class="container">
                    <div class="footer__row">
                        <a href="#" class="footer__logo">
                            <img src="img/footer__logo.png" class="img-fluid" alt="">
                        </a>
                        <ul class="footer__nav">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Company info</a></li>
                            <li><a href="#">How to start?</a></li>
                        </ul>
                        <ul class="footer__social">
                            <li><a href="#"><i class="fab fa-vk"></i></a></li>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-whatsapp"></i></a></li>
                            <li><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
                        </ul>
                    </div>
                    <div class="footer__text">
                        © 2019 VexaGlobal.com<br/>
                        All rights reserved.
                    </div>
                </div>
            </footer>

        </div>


        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>

