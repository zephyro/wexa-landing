<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap&subset=cyrillic" rel="stylesheet">
        
        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="fonts/fontawesome5/css/all.min.css">
        <link rel="stylesheet" href="css/main.css">

    </head>
    <body>

        <div class="page">

            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <a class="header__logo" href="/">
                            <img src="img/logo.png" class="img-fluid" alt="">
                        </a>
                        <ul class="header__nav">
                            <li class="active"><a href="#"><span>Home</span></a></li>
                            <li><a href="#"><span>Company info</span></a></li>
                            <li><a href="#"><span>How to start?</span></a></li>
                        </ul>
                    </div>
                </div>
            </header>

            <section class="main main_first">
                <div class="container">
                    <div class="main_box__row">
                        <div class="main_box">
                            <h3>What You Get With Vexa?</h3>
                            <p>
                                As an Independent Partner of our team, you will not only recieve our support and guidance, but you will also receive this very website that you are currently on! You will be able to have your profile, just like mine. You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started.<br/>
                                You will not get this gift anywhere else, but here! Once you register here with your email address, we will send you some information on how to get started.
                            </p>
                            <div class="user">
                                <div class="user__photo">
                                    <img src="img/user_avatar.png" class="img-fluid" alt="">
                                </div>
                                <div class="user__info">
                                    <div class="user__name">Krys Kriss</div>
                                    <div class="user__staff">Independent Partner</div>
                                    <div class="user__text">Entrepreneur, Business Owner, and Independent Partner of Vexa Global.</div>
                                </div>
                            </div>
                        </div>
                        <div class="main_box">
                            <h3>Let Me Help You Get a Head Start</h3>
                            <p>Once you register here, this will only send you the information you need to get started with EXP ASSET and you will be redirected to the EXP ASSET Official registration Form for the Dashboard Tool that you will be using.</p>
                            <div class="warning mb_30">The Company Name and Tax Fields are OPTIONAL on the EXP ASSET TOOL registration form</div>
                            <form class="form">
                                <div class="form_group">
                                    <label class="form_label">First Name:</label>
                                    <input type="text" class="form_control" name="name" placeholder="Dmytro">
                                </div>
                                <div class="form_group mb_40">
                                    <label class="form_label">E-mail:</label>
                                    <input type="text" class="form_control" name="email" placeholder="dkarts.studio@gmail.com">
                                </div>
                                <button class="btn btn_md btn_shadow btn_long" type="submit">Yes, I need all the help I can get!</button>
                            </form>
                        </div>
                    </div>
                    <ul class="contacts">
                        <li class="contacts_telegram">
                            <div class="contacts__title">Telegram</div>
                            <a href="#">Message Me​​​​​​​</a>
                            <i class="fab fa-telegram-plane"></i>
                        </li>
                        <li class="contacts_whatsapp">
                            <div class="contacts__title">Watsapp</div>
                            <a href="#">Message Me​​​​​​​</a>
                            <i class="fab fa-whatsapp"></i>
                        </li>
                        <li class="contacts_instagram">
                            <div class="contacts__title">Instagram</div>
                            <a href="#">Message Me​​​​​​​</a>
                            <i class="fab fa-instagram"></i>
                        </li>
                        <li class="contacts_facebook">
                            <div class="contacts__title">Facebook</div>
                            <a href="#">Message Me​​​​​​​</a>
                            <i class="fab fa-facebook-square"></i>
                        </li>
                        <li class="contacts_weChat">
                            <div class="contacts__title">WeChat</div>
                            <a href="#">Message Me​​​​​​​</a>
                            <i class="fab fa-weixin"></i>
                        </li>
                    </ul>
                </div>
            </section>

            <footer class="footer">
                <div class="container">
                    <div class="footer__row">
                        <a href="#" class="footer__logo">
                            <img src="img/footer__logo.png" class="img-fluid" alt="">
                        </a>
                        <ul class="footer__nav">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Company info</a></li>
                            <li><a href="#">How to start?</a></li>
                        </ul>
                        <ul class="footer__social">
                            <li><a href="#"><i class="fab fa-vk"></i></a></li>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-whatsapp"></i></a></li>
                            <li><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
                        </ul>
                    </div>
                    <div class="footer__text">
                        © 2019 VexaGlobal.com<br/>
                        All rights reserved.
                    </div>
                </div>
            </footer>

        </div>


        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
