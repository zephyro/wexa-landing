
// Tabs

(function() {

    $('.faq__question').on('click touchstart', function(e){
        e.preventDefault();

        var box = $(this).closest('.faq');
        box.toggleClass('open');
        box.find('.faq__answer').slideToggle('fast');
    });

}());
